package com.everis.ordendetalle.controller.resource;

import java.math.BigDecimal;
import lombok.Data;


@Data
public class OrdenRedResource {

	private Long idCliente;
	private Long idVendedor;
	private String fechaOrden;
	private BigDecimal total;
	private BigDecimal igv;

}
