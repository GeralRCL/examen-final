package com.everis.ordendetalle.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.everis.ordendetalle.controller.resource.OrdenRedResource;
import com.everis.ordendetalle.controller.resource.OrdenResource;
import com.everis.ordendetalle.dao.ClienteClient;
import com.everis.ordendetalle.dao.VendedorClient;
import com.everis.ordendetalle.model.entity.Cliente;
import com.everis.ordendetalle.model.entity.Orden;
import com.everis.ordendetalle.model.entity.Vendedor;
import com.everis.ordendetalle.service.OrdenService;



@RestController
public class ApiController {
	
	
	@Autowired
	OrdenService ordenService;
	
	
	@Autowired
	ClienteClient clienteClient;
	
	
	@Autowired
	VendedorClient vendedorClient;
	
	
	/*
	 * 		CREAR ORDEN
	 */
	@PostMapping("/orden")
	public OrdenResource guardarOrden(@RequestBody OrdenRedResource request) throws Exception {
		/*		REQUEST		*/
		Cliente cliente = clienteClient.obtenerClientePorId(request.getIdCliente());
		Vendedor vendedor = vendedorClient.obtenerVendedorPorId(request.getIdVendedor());
		Orden nuevo = new Orden();
		nuevo.setIdCliente(cliente);
		nuevo.setIdVendedor(vendedor);
		nuevo.setFechaOrden(request.getFechaOrden());
		nuevo.setIgv(request.getIgv());
		nuevo.setTotal(request.getTotal());
		Orden orden = ordenService.insertar(nuevo);
		/*		RESPONSE		*/
		OrdenResource ordenResource = new OrdenResource();
		ordenResource.setId(orden.getId());
		ordenResource.setIdCliente(orden.getIdCliente().getId());
		ordenResource.setIdVendedor(orden.getIdVendedor().getId());
		ordenResource.setFechaOrden(orden.getFechaOrden());
		ordenResource.setTotal(orden.getTotal());
		ordenResource.setIgv(orden.getIgv());
		return ordenResource; 
	}
}
