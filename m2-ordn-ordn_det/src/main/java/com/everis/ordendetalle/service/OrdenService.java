package com.everis.ordendetalle.service;

import com.everis.ordendetalle.model.entity.Orden;

public interface  OrdenService {
			
	public Orden insertar(Orden orden) throws Exception;
	
}
