package com.everis.ordendetalle.dao;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.everis.ordendetalle.model.entity.Cliente;
import org.springframework.cloud.openfeign.FeignClient;


//@FeignClient("MICROSERVICIO_4")
@FeignClient("MICRO4CLIENTE")
public interface ClienteClient {

	@GetMapping("/cliente/{id}")
	Cliente obtenerClientePorId(@PathVariable("id") Long id) throws Exception;
}
