package com.everis.ordendetalle.dao;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.everis.ordendetalle.model.entity.Vendedor;
import org.springframework.cloud.openfeign.FeignClient;


//@FeignClient("MICROSERVICIO_1")
@FeignClient("MICRO1VENDEDORPROD")
public interface VendedorClient {

	@GetMapping("/vendedor/{id}")
	Vendedor obtenerVendedorPorId(@PathVariable("id") Long id) throws Exception;
}
