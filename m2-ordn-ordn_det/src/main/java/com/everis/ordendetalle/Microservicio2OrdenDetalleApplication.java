package com.everis.ordendetalle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication
@EnableFeignClients
public class Microservicio2OrdenDetalleApplication {

	public static void main(String[] args) {
		SpringApplication.run(Microservicio2OrdenDetalleApplication.class, args);
	}

}
