package com.everis.vendedorproducto.service;

import com.everis.vendedorproducto.model.entity.VendedorProducto;

public interface VendedorProductoService {
	
	public VendedorProducto insertar(VendedorProducto vendedorProducto) throws Exception;
	
}
