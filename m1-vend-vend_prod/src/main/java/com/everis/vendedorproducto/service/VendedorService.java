package com.everis.vendedorproducto.service;

import com.everis.vendedorproducto.model.entity.Vendedor;

public interface  VendedorService {
		
	public Iterable<Vendedor> obtenerVendedores();
	
	public Vendedor insertar(Vendedor vendedor) throws Exception;;
	
	public Vendedor obtenerVendedorPorId(Long id) throws Exception;

	
}
