package com.everis.vendedorproducto.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.everis.vendedorproducto.model.entity.VendedorProducto;
import com.everis.vendedorproducto.model.repository.VendedorProductoRepository;
import org.springframework.stereotype.Service;

@Service
public class VendedorProductoServiceImpl implements VendedorProductoService{

	@Autowired
	private VendedorProductoRepository vendedorProductoRepository;
	
	@Override
	public VendedorProducto insertar(VendedorProducto vendedorProducto) throws Exception {
		return vendedorProductoRepository.save(vendedorProducto);
	}
}
