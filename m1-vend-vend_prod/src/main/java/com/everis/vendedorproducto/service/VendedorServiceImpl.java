package com.everis.vendedorproducto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.everis.vendedorproducto.model.entity.Vendedor;
import com.everis.vendedorproducto.model.repository.VendedorRepository;



@Service
public class VendedorServiceImpl implements VendedorService{
	
	@Autowired
	private VendedorRepository vendedorRepository;
	
	@Override
	public Iterable<Vendedor> obtenerVendedores() {
		return vendedorRepository.findAll();
	}

	@Override
	public Vendedor insertar(Vendedor vendedor) throws Exception {
		return vendedorRepository.save(vendedor);
	}

	@Override
	public Vendedor obtenerVendedorPorId(Long id) throws Exception {
		return vendedorRepository.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Vendedor no encontrado"));
	}
	
}
