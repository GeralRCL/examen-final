package com.everis.vendedorproducto.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.vendedorproducto.model.entity.VendedorProducto;

@Repository
public interface VendedorProductoRepository extends CrudRepository<VendedorProducto, Long> {

}