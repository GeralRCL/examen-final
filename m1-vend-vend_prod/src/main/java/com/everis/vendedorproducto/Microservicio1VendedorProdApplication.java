package com.everis.vendedorproducto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class Microservicio1VendedorProdApplication {

	public static void main(String[] args) {
		SpringApplication.run(Microservicio1VendedorProdApplication.class, args);
	}

}
