package com.everis.vendedorproducto.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.everis.vendedorproducto.controller.resource.VendedorProductoRedResource;
import com.everis.vendedorproducto.controller.resource.VendedorProductoResource;
import com.everis.vendedorproducto.controller.resource.VendedorRedResource;
import com.everis.vendedorproducto.controller.resource.VendedorResource;
import com.everis.vendedorproducto.dao.ProductoClient;
import com.everis.vendedorproducto.model.entity.Producto;
import com.everis.vendedorproducto.model.entity.Vendedor;
import com.everis.vendedorproducto.model.entity.VendedorProducto;
import com.everis.vendedorproducto.service.VendedorProductoService;
import com.everis.vendedorproducto.service.VendedorService;



@RestController
public class ApiController {
	
	
	@Autowired
	VendedorService vendedorService;
	
	@Autowired
	VendedorProductoService vendedorProductoService;
	
	@Autowired
	ProductoClient productoClient;
	
	
	/*
	 * 	BTENER VENDEDOR POR ID
	 */
	@GetMapping("/vendedor/{id}")
	public VendedorResource obtenerVendedorPorId(@PathVariable("id") Long id) throws Exception {
		Vendedor vendedor = vendedorService.obtenerVendedorPorId(id);
		VendedorResource vendedorResource = new VendedorResource();
		vendedorResource.setId(vendedor.getId());
		vendedorResource.setNombres(vendedor.getNombres());
		vendedorResource.setApellidos(vendedor.getApellidos());
		vendedorResource.setCargo(vendedor.getCargo());
		return vendedorResource;
	}
	
	
	
	/*
	 * 		CREAR VENDEDOR
	 */
	@PostMapping("/vendedor")
	public VendedorResource guardarVendedor(@RequestBody VendedorRedResource request) throws Exception {
		/*		REQUEST		*/
		Vendedor nuevo = new Vendedor();
		nuevo.setNombres(request.getNombres());
		nuevo.setApellidos(request.getApellidos());
		nuevo.setCargo(request.getCargo());
		Vendedor vendedor = vendedorService.insertar(nuevo);
		/*		RESPONSE		*/
		VendedorResource vendedorResource = new VendedorResource();
		vendedorResource.setId(vendedor.getId());
		vendedorResource.setNombres(vendedor.getNombres());
		vendedorResource.setApellidos(vendedor.getApellidos());
		vendedorResource.setCargo(vendedor.getCargo());
		return vendedorResource; 
	}

	
	
	/*
	 * 		CREAR VENDEDOR_PRODUCTO
	 */
	@PostMapping("/vendedorProducto")
	public VendedorProductoResource guardarVendedorProducto(@RequestBody VendedorProductoRedResource request) throws Exception {
		/*		REQUEST		*/
		Producto producto = productoClient.obtenerProductoPorId(request.getIdProducto());
		Vendedor vendedor = vendedorService.obtenerVendedorPorId(request.getIdVendedor());
		System.out.print(" ------------------------------");
		VendedorProducto nuevo = new VendedorProducto();
		nuevo.setIdProducto(producto);
		nuevo.setIdVendedor(vendedor);
		nuevo.setPrecio(request.getPrecio());
		VendedorProducto vendedorProducto = vendedorProductoService.insertar(nuevo);
		System.out.print(" ------------------------------");
		/*		RESPONSE		*/
		VendedorProductoResource vendedorProductoResource = new VendedorProductoResource();
		vendedorProductoResource.setId(vendedorProducto.getId());
		vendedorProductoResource.setIdProducto(vendedorProducto.getIdProducto().getId());
		vendedorProductoResource.setIdVendedor(vendedorProducto.getIdVendedor().getId());
		vendedorProductoResource.setPrecio(vendedorProducto.getPrecio());
		return vendedorProductoResource; 
	}

}
