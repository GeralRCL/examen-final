package com.everis.vendedorproducto.controller.resource;

import lombok.Data;

@Data
public class VendedorResource {

	private Long id;
	private String nombres;
	private String apellidos;
	private String cargo;
}
