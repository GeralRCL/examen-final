package com.everis.vendedorproducto.controller.resource;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class VendedorProductoResource {

	private Long id;
	private Long idProducto;
	private Long idVendedor;
	private BigDecimal precio;
}
