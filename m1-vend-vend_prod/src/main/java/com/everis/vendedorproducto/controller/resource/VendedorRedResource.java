package com.everis.vendedorproducto.controller.resource;

import lombok.Data;

@Data

public class VendedorRedResource {
	private String nombres;
	private String apellidos;
	private String cargo;
}