package com.everis.vendedorproducto.dao;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.everis.vendedorproducto.model.entity.Producto;
import org.springframework.cloud.openfeign.FeignClient;


@FeignClient("MICRO3PRODUCTOS")
//@FeignClient("MICROSERVICIO_3")
public interface ProductoClient {
	
	@GetMapping("/producto/{id}")
	Producto obtenerProductoPorId(@PathVariable("id") Long id) throws Exception;
	
}
