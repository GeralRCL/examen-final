package com.everis.producto.controller.resource;

import lombok.Data;

@Data
public class ProductoRedResource {
	private String nombre;
	private String descripcion;
	private String unidadMedida;
}