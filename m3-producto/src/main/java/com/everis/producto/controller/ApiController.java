package com.everis.producto.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.everis.producto.controller.resource.ProductoRedResource;
import com.everis.producto.controller.resource.ProductoResource;
import com.everis.producto.model.entity.Producto;
import com.everis.producto.service.ProductoService;


@RestController
public class ApiController {
	
	
	
	@Autowired
	ProductoService productoService;
	
	
	
	/*
	 *   OBTENER PRODUCTO POR ID
	 */
	@GetMapping("/producto/{id}")
	public ProductoResource obtenerProductoPorId(@PathVariable("id") Long id) throws Exception {
		Producto producto = productoService.obtenerProductoPorId(id);
		ProductoResource productoResource = new ProductoResource();
		productoResource.setId(producto.getId());
		productoResource.setNombre(producto.getNombre());
		productoResource.setDescripcion(producto.getDescripcion());
		productoResource.setUnidadMedida(producto.getUnidadMedida());
		return productoResource;
	}
	
	
	
	/*
	 *    INSERTAR PRODUCTO
	 */
	@PostMapping("/producto")
	public ProductoResource guardarProducto(@RequestBody ProductoRedResource request) throws Exception {
		/*		REQUEST		*/
		Producto nuevo = new Producto();
		nuevo.setNombre(request.getNombre());
		nuevo.setDescripcion(request.getDescripcion());
		nuevo.setUnidadMedida(request.getUnidadMedida());
		Producto producto = productoService.insertar(nuevo);
		/*		RESONSE		*/
		ProductoResource productoResource = new ProductoResource();
		productoResource.setId(producto.getId());
		productoResource.setNombre(producto.getNombre());
		productoResource.setDescripcion(producto.getDescripcion());
		productoResource.setUnidadMedida(producto.getUnidadMedida());
		return productoResource; 
	}
	

}
