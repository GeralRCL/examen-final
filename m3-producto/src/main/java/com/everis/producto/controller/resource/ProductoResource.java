package com.everis.producto.controller.resource;

import lombok.Data;

@Data
public class ProductoResource {

	private Long id;
	private String nombre;
	private String descripcion;
	private String unidadMedida;
}
