package com.everis.producto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Microservicio3ProductoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Microservicio3ProductoApplication.class, args);
	}

}
