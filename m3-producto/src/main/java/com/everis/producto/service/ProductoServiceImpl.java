package com.everis.producto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import com.everis.producto.model.entity.Producto;
import com.everis.producto.model.repository.ProductoRepository;


@Service
public class ProductoServiceImpl implements ProductoService{

	
	@Autowired
	private ProductoRepository productoRepository;

	@Override
	public Producto insertar(Producto producto) throws Exception {
		return productoRepository.save(producto);
	}

	@Override
	public Producto obtenerProductoPorId(Long id) throws Exception {
		return productoRepository.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Producto no encontrado"));
	}

	
}
