package com.everis.producto.service;

import com.everis.producto.model.entity.Producto;

public interface  ProductoService {
	
	public Producto insertar(Producto producto) throws Exception;;
	
	public Producto obtenerProductoPorId(Long id) throws Exception;

	
}
