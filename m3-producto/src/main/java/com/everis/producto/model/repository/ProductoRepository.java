package com.everis.producto.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.producto.model.entity.Producto;

@Repository
public interface ProductoRepository extends CrudRepository<Producto, Long> {

}
