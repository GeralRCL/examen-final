package com.everis.cliente.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.everis.cliente.model.entity.Cliente;
import com.everis.cliente.model.repository.ClienteRepository;

@Service
public class ClienteServiceImpl implements ClienteService{

	
	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public Cliente insertar(Cliente cliente) throws Exception {
		return clienteRepository.save(cliente);
	}

	@Override
	public Cliente obtenerClientePorId(Long id) throws Exception {
		return clienteRepository.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Cliente no encontrado"));
	}
	
}
