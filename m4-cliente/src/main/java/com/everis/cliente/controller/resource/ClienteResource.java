package com.everis.cliente.controller.resource;

import lombok.Data;

@Data
public class ClienteResource {

	private Long id;
	private String nombres;
	private String apellidos;
	private String direccion;
}
