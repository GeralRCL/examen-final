package com.everis.cliente.controller.resource;

import lombok.Data;

@Data
public class ClienteRedResource {
	
	private String nombres;
	private String apellidos;
	private String direccion;
	
}