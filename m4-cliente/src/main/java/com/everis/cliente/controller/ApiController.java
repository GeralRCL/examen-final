package com.everis.cliente.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.everis.cliente.controller.resource.ClienteRedResource;
import com.everis.cliente.controller.resource.ClienteResource;
import com.everis.cliente.model.entity.Cliente;
import com.everis.cliente.service.ClienteService;



@RestController
public class ApiController {
	
	
	
	@Autowired
	ClienteService clienteService;
	
	
	/*
	 * OBTNER CLIENTE POR ID
	 */
	@GetMapping("/cliente/{id}")
	public ClienteResource obtenerPorId(@PathVariable("id") Long id) throws Exception {
		Cliente cliente = clienteService.obtenerClientePorId(id);
		ClienteResource clienteResource = new ClienteResource();
		clienteResource.setId(cliente.getId());
		clienteResource.setNombres(cliente.getNombres());
		clienteResource.setApellidos(cliente.getApellidos());
		clienteResource.setDireccion(cliente.getDireccion());
		return clienteResource;
	}
	
	
	/*
	 * INSERT CLIENTE
	 */
	@PostMapping("/cliente")
	public ClienteResource crearCliente(@RequestBody ClienteRedResource request) throws Exception {
		/*    request     */
		Cliente nuevo = new Cliente();
		nuevo.setNombres(request.getNombres());
		nuevo.setApellidos(request.getApellidos());
		nuevo.setDireccion(request.getDireccion());
		Cliente cliente = clienteService.insertar(nuevo);			
		/*   RESPONSE    */
		ClienteResource clienteResource = new ClienteResource();
		clienteResource.setId(cliente.getId());
		clienteResource.setNombres(cliente.getNombres());
		clienteResource.setApellidos(cliente.getApellidos());
		clienteResource.setDireccion(cliente.getDireccion());
		return clienteResource; 
	}


}
