package com.everis.cliente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Microservicio4ClienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(Microservicio4ClienteApplication.class, args);
	}

}
